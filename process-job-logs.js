const Promise = require('bluebird');
const path = require('path');
const fs = require('fs-extra');
const readFile = Promise.promisify(fs.readFile);
const readdir = Promise.promisify(fs.readdir);
const escapeStringRegexp = require('escape-string-regexp');



const jobLogDir = './jobs/';


const opts = require('yargs')
	.option('grep', {
		alias: 'g',
		required: true,
		description: 'The regex filter to match against the log text'
	})
	.option('escape-grep', {
		type: 'boolean',
		required: false,
		description: 'Whether to escape the grep string (normally interpreted as regex)',
		default: false,
	})
	.option('multiline', {
		type: 'boolean',
		required: false,
		description: 'Whether to add the multiline flag to to the grep regex',
		default: false,
	})
	.option('ascending', {
		type: 'boolean',
		required: false,
		description: 'Whether to start processing the newest files first',
		default: false,
	})
  .option('concurrency', {
    alias: 'c',
    required: false,
    description: 'Number of files to read at once',
		default: Infinity,
  })
  .help('help')
  .alias('help', 'h')
  .argv;



var logTextFilterRegex = new RegExp(opts.escapeGrep ? escapeStringRegexp(opts.grep) : opts.grep, `i${opts.multiline ? 'm' : ''}`);

console.log('Filtering regex', logTextFilterRegex);

readdir(jobLogDir)
	.then((files) => {
		console.log(`Looking through ${files.length} files`);

		let filesToProcess = files;
		if (opts.ascending) {
			filesToProcess = files.reverse();
		}

		console.log('Processing started...');
		Promise.map(filesToProcess, (fileName) => {
			return readFile(path.join(jobLogDir, fileName), 'utf8')
				.then((logText) => {
					if(logText.match(logTextFilterRegex)) {
						console.log(fileName);
					}
				});
		}, { concurrency: opts.concurrency });
	});
