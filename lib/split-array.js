// Splits an array (not sequential)
module.exports = function splitArray(arr, numberOfPieces) {
  return arr.reduce((chunks, item, index) => {
    chunks[index % numberOfPieces] = chunks[index % numberOfPieces] || [];
    chunks[index % numberOfPieces].push(item);
    return chunks;
  }, []);
};
