# Grep(search) in GitLab Pipelines and Jobs

Quick and dirty script to fetch GitLab log output from jobs and search for strings.

Ideally this would be in GitLab itself, give this issue some :+1: love, https://gitlab.com/gitlab-org/gitlab-ce/issues/34089

## Setup

Get a [GitLab personal access token](https://docs.gitlab.com/ee/api/README.html#personal-access-tokens) and save it to `config.json` or pass it in as a command line argument, `--gitlab-access-token`.

`config.json`
```json
{
	"gitlabAccessToken": "foobarbaz"
}
```


## Scripts


### `fetch-job-logs.js`

Paginate from the starting page to the stop date and grab all of the jobs during that time. Fetch if necessary and save to `./jobs/:id.txt`

Currently only looks through `failed` jobs.

```
node fetch-job-logs.js --project-id gitlab-org%2Fgitlab-ce --start-page 10 --stop-date 2017-06-20T00:00:00.000Z --concurrency=3
```


### `process-job-logs.js`

Go through all of the files in `./jobs/` and grep for the given value. Or just use your favorite grep flavor on the `./jobs/*` files.

```
node process-job-logs.js -g "undefined is not an object (evaluating 'modules[moduleId].call')" --escape-grep
```
